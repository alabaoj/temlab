# Promtail

Promtail is the agent responsible for collecting logs from pods and forward same to Loki.
It will scrape Kubernetes logs and add label metadata before sending it to Loki via an API.
Promtail's metadata addition is exactly the same as Prometheus, so you will end up with the exact same labels for your resources.

Install Promtail as described [here](/helm_components/README.md#deploymon)

Source: https://github.com/grafana/loki/tree/master/production/helm

[Here](https://grafana.com/docs/features/datasources/loki/) is the doc on using Loki in Grafana
