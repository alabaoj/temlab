
# Loki

Loki is the main server which aggregates and stores logs. It processes queries and pushes results to the dashboard.

* Install it as described [here](/helm_components/README.md#deploymon)

* Install also Grafana and Promtail as described [here](/helm_components/README.md#deploymon)

Source: https://github.com/grafana/loki/tree/master/production/helm


## Usage
 
 Get the admin password for the Grafana pod by running:

```bash
$ kubectl get secret --namespace base base-grafana-logging -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

To access the Grafana UI, run the following command:

```bash
$ kubectl port-forward --namespace base service/base-grafana-logging 3001:80
```
Navigate to http://localhost:3001 and login with `admin` and the password output above.

### Adding data sources

Before you create your first dashboard you need to add your data sources. The default datasource is already added based on the settings in [`values-grafana-logging.yaml`](/helm_components/deploy/base/values-grafana-logging.yaml).
You may click on the configurations icon and add more datasources.

NOTE: Grafana v6.5.x supports saving of provisioned UI from the dashboard

See [here](https://grafana.com/docs/administration/provisioning/#datasources) for more on datasources

### Querying logs

Querying and displaying log data from Loki is available via Explore (select Explore icon on the menu bar to the left).
Select the base-loki (or any other) data source, and then enter a log query in the query field to explore and display your logs. 

A log query consists of two parts: log stream selector ( in curly braces ) and a search expression.

In the examples below, on the left of the matching operators are the labels part while the search expressions (can be regex or plain text) are on the right.
```
{log streams selector} |= "search expression"
```
Examples are:
```
{k8s_app="kubernetes-dashboard"} |~ "error"

```
```
{namespace="base", app="rook-ceph-operator"} != "I"
```
```
{stream="stderr"} |= "error"

```
The following filter types are currently supported for search expression:
* |= line contains string.
* != line doesn’t contain string.
* |~ line matches regular expression.
* !~ line does not match regular expression.

The following label matching operators are currently supported for logs stream selector:
* = exactly equal.
* != not equal.
* =~ regex-match.
* !~ do not regex-match


For performance reasons you need to start by choosing a log stream by selecting a log label.

[Here](https://grafana.com/docs/features/datasources/loki/) is the doc on using Loki in Grafana


