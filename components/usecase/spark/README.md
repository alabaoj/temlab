# Using Spark from the cluster

## Needed resources
The needed resources (service account, role, role binding, headless service and deployment) have already been created (see the current chart and the "serviceaccounts-namespace" chart in this layer).

Remark: the deployment creates a pod based on the Spark docker container from the Data Reply repository. Just in case the Dockerfile for that container is also [in this repository](../../../../../apps/spark/Dockerfile).

## Running a Spark app
You will have to list all pods and find the pod name that will start with "spark-":
```
kubectl get pods -n dap-core
```
Wait for the pod to have the status "RUNNNG", then start a shell in the container and navigate to /opt/spark in it:
```
kubectl exec -it -n dap-core SPARK_POD_ID -- /bin/bash
cd /opt/spark
```

Then either or:
1. Submit a spark-job, using the example jars:
```
bin/spark-submit \
    --master k8s://https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT \
    --deploy-mode client \
    --name spark-pi \
    --class org.apache.spark.examples.SparkPi \
    --conf spark.executor.instances=1 \
    --conf spark.driver.port=4040 \
    --conf spark.driver.host=spark.dap-core \
    --conf spark.kubernetes.driver.label.app=spark-service \
    --conf spark.kubernetes.container.image=datareply/spark:latest \
    --conf spark.kubernetes.authenticate.driver.serviceAccountName=poweruser-user-dap-core \
    --conf spark.kubernetes.namespace=dap-core \
    --conf spark.eventLog.enabled=true \
    --conf spark.eventLog.dir=file:///spark-event-logs/ \
    --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.options.claimName=spark-history-server-pvc \
    --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.mount.path=/spark-event-logs \
    --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.mount.readOnly=false \
    --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.options.claimName=spark-history-server-pvc \
    --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.mount.path=/spark-event-logs \
    --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.mount.readOnly=false \
    examples/jars/spark-examples_2.11-2.4.0.jar 
```

Check `Spark History Server` section bellow for explanation of eventLog and kubernetes volumes related configurations.

2. Start the spark shell and execute some actions(b)
```
bin/spark-shell \
    --master k8s://https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT \
    --deploy-mode client \
    --conf spark.executor.instances=1 \
    --conf spark.driver.port=4040 \
    --conf spark.driver.host=spark.dap-core \
    --conf spark.kubernetes.driver.label.app=spark-service \
    --conf spark.kubernetes.container.image=datareply/spark:latest \
    --conf spark.kubernetes.authenticate.driver.serviceAccountName=poweruser-user-dap-core \
    --conf spark.kubernetes.namespace=dap-core \
    --conf spark.eventLog.enabled=true \
    --conf spark.eventLog.dir=file:///spark-event-logs/ \
    --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.options.claimName=spark-history-server-pvc \
    --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.mount.path=/spark-event-logs \
    --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.mount.readOnly=false \
    --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.options.claimName=spark-history-server-pvc \
    --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.mount.path=/spark-event-logs \
    --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.mount.readOnly=false
```

Check `Spark History Server` section bellow for explanation of eventLog and kubernetes volumes related configurations.

You can use the Scala example here:
```	
val count = sc.parallelize(1 to 5).filter { _ =>
  val x = math.random
  val y = math.random
  x*x + y*y < 1
}.count()
println(s"Pi is roughly ${4.0 * count / 5}")
```

## Spark History Server

We are using PVC as the backing storage for Spark history events. This means that our Spark applications should mount a PVC in the driver and executor containers and use it for storing Spark event logs. On the other side, Spark History Server pod should also mount the same PVC and consume event logs from there for displaying in UI.

For a discussion on event logs storage options check [Discussions of Storage Options](https://github.com/helm/charts/tree/master/stable/spark-history-server#discussions-of-storage-options).

From the Spark applicatoin side the following configurations need to be set to generate event logs (source: [Enabling spark-submit to log events for PVC](https://github.com/helm/charts/tree/master/stable/spark-history-server#pvc)):

- `spark.eventLog.enabled=true`: to enable generation of event logs

- `spark.eventLog.dir=file:///spark-event-logs/`: to specify the location of event logs. It should point to the location of mounted PVC that is set later. The value should be equal to what is specified in `components\deploy\dap-core\values.yaml` file as `spark.eventLog.mountPath`, so that event logs of applications started in client mode are also visible.

- `spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.options.claimName=spark-history-server-pvc`: this specifies an existing PVC that should be mounted to the driver container for writing event logs. It should point to the same PVC that Spark History Server is consuming from.

- `spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.mount.path=/spark-event-logs`: specifies the path where PVC will be mounted in driver container.

- `spark.kubernetes.driver.volumes.persistentVolumeClaim.checkpointpvc.mount.readOnly=false`: makes sure that Spark application can write to mounted volume.

- `spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.options.claimName=spark-history-server-pvc`: this specifies an existing PVC that should be mounted to executor container for writing event logs. It should point to the same PVC that Spark History Server is consuming from.

- `spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.mount.path=/spark-event-logs`: specifies the path where PVC will be mounted in executor containers.

- `spark.kubernetes.executor.volumes.persistentVolumeClaim.checkpointpvc.mount.readOnly=false`: makes sure that Spark application can write to mounted volume.


There is a slight differences when executing Spark application in client and cluster modes. For execution in client mode we have to make sure that PVC for event logs is also mounted to the pod container from where we execute `spark-submit` command. For this we added mounting of the PVC to the deployment of `spark` chart. Check parameters of `spark` chart for more informatoin on how this is done. We also have to make sure that the mounting directory is the same to what we set in `spark.eventLog.dir`.
