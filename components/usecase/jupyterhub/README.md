# Jupyterhub

The version of Jupyterhub deployed with the ECF-Namespace will be able to interact with Spark since we use the image datareply/jupyterhub:0.8.1_withSpark2.4.4_Hadoop2.7 (Dockerfile [here](/apps/jupyterhub/Dockerfile_withSpark2.4.4_Hadoop2.7)). Note that the `spark.driver.host` has to be set to the local IP of the Jupyter notebook, that can be obtained by calling `hostname -i` or the Python code:

```python
import socket
socket.gethostbyname(socket.gethostname())
```

## Usage of Spark with Jupyterhub

Here is a simple example of using spark with Jupyter:

First launch a new Jupyter Notebook instance with JupyterHub.

Execute now the following in the Notebook Cells:

1. First get the Spark Context

```python
import pyspark
import os
import socket

kubernetes_host=os.environ['KUBERNETES_SERVICE_HOST']
kubernetes_port=os.environ['KUBERNETES_SERVICE_PORT']

conf = pyspark.SparkConf()
conf.setMaster("k8s://https://%s:%s" %(kubernetes_host,kubernetes_port))
conf.set("spark.executor.instances", "2")
conf.set("spark.driver.port", "4040")
conf.set("spark.driver.host", socket.gethostbyname(socket.gethostname()) )
conf.set("spark.kubernetes.driver.label.app", "spark-service")
conf.set("spark.kubernetes.container.image", "datareply/pyspark:0.0.3_python3.7_pyspark2.4.4")
conf.set("spark.kubernetes.authenticate.driver.serviceAccountName", "poweruser-user-ecf")
conf.set("spark.kubernetes.namespace", "ecf")

sc = pyspark.SparkContext.getOrCreate(conf=conf)
```

2. Now run an example Spark Job:

```python
from operator import add
import random

partitions = 8
n = 1000000 * partitions

def f(_):
    x = random.random() * 2 - 1
    y = random.random() * 2 - 1
    return 1 if x ** 2 + y ** 2 <= 1 else 0

count = sc.parallelize(range(1, n + 1), partitions).map(f).reduce(add)
print("Pi is roughly %f" % (4.0 * count / n))
```
