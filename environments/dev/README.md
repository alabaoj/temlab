# Helm Components

## Structure

The _components/_ folder contains all the necessary Helm Charts for deploying the base of the platform as well as the the usecase specific components.
The _environments/dev/_ folder contains all the helmfiles and values files for installing the basis of the platform (_/dap-base_) and the specific components for one use case (example folder _/usecase_)

![Logical layering](../../docs/helm/Dap.png)

### 1. Dap-base

The dap-base namespace will constitute the basis of the platform having the storage and monitoring components in place. A high level presentation of the components in dap-base can be seen below.

![dap-base](../../docs/helm/dap-base.png)

### 2. Usecase

Each usecase will have a its own separate namespace and will make use of specific services.

The available services will be:
   - Postgres
   - Nifi (along with Zookeeper)
   - JupyterHub
   - Spark (along with Spark History Server)
   - Docker Registry

## Installation

The installation requires:
   - [Helm](https://helm.sh/docs/intro/install/) version >= 3
   - [Helmfile](https://github.com/roboll/helmfile#installation) 
   - [Helm diff plugin](https://github.com/databus23/helm-diff#install) 

The installation will make use of all the charts present in the /components folder which can be reused per namespace and the helmfiles which are located in _/environments/dev/{namespace}_ 

### Install dap-base

```
helmfile -f environments/dev/dap-base/helmfile.yaml apply
```

The dap-base deployment is however split into multiple logical groups therefore the groups can be installed separately and furthermore, each component can be selected to be installed individually.

Example of how the master helmfile installs the components step by step and how they can be also done manually

1. install only the resources (namespace and serviceaccounts)
```
helmfile -f environments/dev/dap-base/resources/helmfile.yaml
```

out of which we can install only the namespace
```
helmfile -f environments/dev/dap-base/resources/helmfile.yaml --selector name=dap-base apply
```

2. install the storage (rook, ceph cluster and ceph storage classes)
```
helmfile -f environments/dev/dap-base/storage/helmfile.yaml apply
```
out of which, as an example we can install only the ceph cluster
```
helmfile -f environments/dev/dap-base/storage/helmfile.yaml --selector name=ceph-cluster apply
```

3. install the monitoring
```
helmfile -f environments/dev/dap-base/storage/helmfile.yaml --selector name=ceph-cluster apply
```

