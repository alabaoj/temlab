
import boto3, botocore, time, os

import datetime, tempfile, logging
from random import random
from random import randrange
from datetime import datetime
from botocore.exceptions import ClientError

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)  

target_dir="/tmp/tempfiles"
endpoint = os.getenv('AWS_HOST')
aws_region = os.getenv('AWS_REGION', default = 'eu-central-1')
bucket_name = os.getenv('BUCKET_NAME')
secret_key = os.getenv('AWS_SECRET_ACCESS_KEY')
access_key = os.getenv('AWS_ACCESS_KEY_ID')
file_count = os.getenv('FILE_COUNT', default = 1000)
file_size = os.getenv('FILE_SIZE', default = 1)

s3 = boto3.resource('s3',  region_name='eu-central-1', api_version=None, use_ssl=False, verify=None, \
     endpoint_url=endpoint, aws_access_key_id=access_key, aws_secret_access_key=secret_key, aws_session_token=None, config=None)


def generate_file(file_size):
    timestamp = int(time.mktime(datetime.now().timetuple()))
    number  = randrange(1000, 9999)
    suffix= str(number)+str(timestamp)+str(number)
    basename = "rook"
    filename = "_".join([basename, suffix]) 
    fd, path = tempfile.mkstemp(filename, 'ceph', target_dir, True )    
    with open(path, 'w') as f:
        f.seek(file_size*1024*1024)
        f.write("X")
    
    os.close(fd)
    

def sync_to_s3():
    if not os.path.isdir(target_dir):
        raise ValueError('target_dir %r not found.' % target_dir)

    try:
        s3.create_bucket(Bucket=bucket_name,
                         CreateBucketConfiguration={'LocationConstraint': aws_region})
    except ClientError:
        pass

    for filename in os.listdir(target_dir):
        logger.warning('Uploading %s to S3 bucket %s' % (filename, bucket_name))
        response = s3.Object(bucket_name, filename).put(Body=open(os.path.join(target_dir, filename), 'rb'))
        logger.info(response)
        logger.info('File uploaded to %s/%s/%s' % (
            endpoint, bucket_name, filename))

        os.remove(os.path.join(target_dir, filename))  


def main():
    for i in range(0, int(file_count)):
        generate_file(int(file_size))
    sync_to_s3()    


if __name__ == '__main__':
    main()
    
