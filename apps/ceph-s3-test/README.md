
# Docker image for testing Ceph S3 object <a name="cephS3ObjectTest"></a>

This directory contains all needed files to create a Docker image with a Python script for testing Ceph S3 object storage.

## Scripts

The python script [`ceph-s3-test.py`](scripts/ceph-s3-test.py) writes a number of files to the Ceph S3 storage.
The script receives two parameters (file_count and file_size(in MiB)), these specify the number of files and the size of each file.
In case additional test scripts are required, just add such in the the `./scripts` folder.

##  Usage

[Here](/benchmarking/ceph_s3_test#ceph-s3-object-testing) is an example on how to use this Docker image.
