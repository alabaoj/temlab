import psycopg2
import sys
import pprint
import time

def main():
    conn_string = "host='dap-core-postgres.dap-core' dbname='jupyter' user='postgres' password='postgres'"
    print("Connecting to database...")

    # get a connection, if a connect cannot be made an exception will be raised here
    conn = psycopg2.connect(conn_string)
    # conn.cursor will return a cursor object, you can use this cursor to perform queries
    cursor = conn.cursor()
   
    # create new table, insert data
    cursor.execute("DROP TABLE IF EXISTS COMPANY")
    cursor.execute("CREATE TABLE COMPANY (ID INT PRIMARY KEY NOT NULL,NAME TEXT NOT NULL, AGE INT NOT NULL, ADDRESS  CHAR(50),SALARY  REAL);")
    for i in range(0, 100):
        cursor.execute("INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) VALUES (" + str(i) + ", 'Teddy', 23, 'Norway', 80000);")
        time.sleep(1)
        # debug print
        print("Executed for " + str(i))
    
    # retrieve the records from the database
    cursor.execute("SELECT * FROM COMPANY")
    records = cursor.fetchall()
    conn.commit()
    # print out the records using pretty print
    # note that the NAMES of the columns are not shown, instead just indexes.
    pprint.pprint(records)

if __name__ == "__main__":
        main()
