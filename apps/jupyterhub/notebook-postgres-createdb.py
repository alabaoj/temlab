import psycopg2
import sys
import pprint

def main():
        conn_string = "host='dap-core-postgres.dap-core' dbname='jupyter' user='postgres' password='postgres'"
        print("Connecting to database...")
        db_name= "jupyter_test_new13"
        table_name= "test_table7"
        # get a connection, if a connect cannot be made an exception will be raised here
        conn = psycopg2.connect(conn_string)
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        conn.autocommit = True
        cursor = conn.cursor()
        cursor.execute('CREATE DATABASE {};'.format(db_name))
        conn.commit()
        conn.close()
        
        #New connection to the previously created DB        
      
        conn_string = "host='dap-core-postgres.dap-core' dbname={} user='postgres' password='postgres'".format(db_name)
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        # execute our Query
        # Replace COMPANY2 with another name if already exists
        
        cursor.execute("CREATE TABLE %s(ID INT PRIMARY KEY NOT NULL,NAME TEXT NOT NULL, AGE INT NOT NULL, ADDRESS  CHAR(50),SALARY  REAL);" % (table_name))
        cursor.execute("INSERT INTO %s (ID,NAME,AGE,ADDRESS,SALARY) VALUES (3, 'Teddy', 23, 'Norway', 80000);" % (table_name))
        cursor.execute("SELECT * FROM %s;" % (table_name))

        # retrieve the records from the database
        records = cursor.fetchall()
        conn.commit()
        # print out the records using pretty print
        # note that the NAMES of the columns are not shown, instead just indexes.
        # for most people this isn't very useful so we'll show you how to return
        # columns as a dictionary (hash) in the next example.
        pprint.pprint(records)

if __name__ == "__main__":
        main()
