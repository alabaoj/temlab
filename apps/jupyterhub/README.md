# Additional files for Jupyterhub
This directory contains additional files for Jupyterhub:
## Dockerfile for the image used
The docker image resides in our docker repository: [datareply/jupyterhub:0.8.1](https://cloud.docker.com/u/datareply/repository/docker/datareply/jupyterhub). It is based on [the official image](https://github.com/jupyterhub/jupyterhub/tree/master/singleuser) and installs additional libraries for working with Postgresql.

### Dockerfile for PySpark

The File `Dockerfile_withSpark2.4.4_Hadoop2.7` is used to generate the image that can be found at [datareply/jupyterhub:0.8.1_withSpark2.4.4_Hadoop2.7](https://cloud.docker.com/u/datareply/repository/docker/datareply/jupyterhub). This image additionally contains files that are necessary to work with Spark. This image also contains tensorflow.

## Python scripts
There are currently two scripts in the folder, one creates a new database and another works with a database that is already there.

To create a new DB in postgres:
```
> kubectl -n dap-core exec -it dap-core-postgres-0 -- /bin/bash
> su - postgres
> psql
psql> create DATABASE jupyter;
```
