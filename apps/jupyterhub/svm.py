import pickle
from pprint import pprint
from sklearn import svm, datasets
from joblib import dump, load

clf = svm.SVC(gamma='scale')
iris = datasets.load_iris()
X, y = iris.data, iris.target
clf.fit(X, y)  
svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape='ovr', degree=3, gamma='scale', kernel='rbf',
    max_iter=-1, probability=False, random_state=None, shrinking=True,
    tol=0.001, verbose=False)

dump(clf, "/home/shared/svm.joblib")


clf = load("/home/shared/svm.joblib") 
s = pickle.dumps(clf)
clf2 = pickle.loads(s)
result = clf2.predict(X)
pprint(result)

# datasets.fetch_20newsgroups()
