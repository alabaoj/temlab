# Folder for Dockerfiles and scripts
This folder is supposed to be used for Dockerfiles and useful scripts for components that are used on the platform, mostly in Helm charts.

**IMPORTANT**:

Please name the Dockerfiles in the form Dockerfile_tagname. That way we can keep track of which Docker files are for which image in case we need to build them

See the subfolders for content and more information.
